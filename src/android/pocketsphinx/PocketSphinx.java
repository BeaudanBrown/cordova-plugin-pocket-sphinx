package pocketsphinx;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class PocketSphinx extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        // Could have multiple actions here for different functions
        if (action.equals("parseAudio")) {
            String message = args.getString(0);
            this.parseAudio(message, callbackContext);
            return true;
        }
        return false;
    }

    private void parseAudio(String message, CallbackContext callbackContext) {
        // Insert some useful Java here
        if (message.equals("succeed")) {
            callbackContext.success("Success!");
        } else {
            callbackContext.error("Message should say succeed");
        }
    }
}
