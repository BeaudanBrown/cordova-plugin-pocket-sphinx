var exec = require('cordova/exec');

// This is the JavaScript wrapper for the plugin where we can define
// the JS functions that Ionic can call to run the Java code
exports.parseAudio = function(arg0, success, error) {
    exec(success, error, "PocketSphinx", "parseAudio", [arg0]);
};
